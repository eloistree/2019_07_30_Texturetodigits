﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirty_PanelsSize : MonoBehaviour
{

    public RectTransform[] m_panels;

    public void SetWidth(float value) {
        for (int i = 0; i < m_panels.Length; i++)
        {
            Rect r =            m_panels[i].rect;
            m_panels[i].sizeDelta= new Vector2( value, r.height);
        }
    }
    public void SetHeight(float value) {
        for (int i = 0; i < m_panels.Length; i++)
        {

            Rect r = m_panels[i].rect;
            m_panels[i].sizeDelta = new Vector2( r.width, value);
        }
    }
    public void SetRotation(float value) {
        for (int i = 0; i < m_panels.Length; i++)
        {
            Vector3 rot =
            m_panels[i].localRotation.eulerAngles;
            m_panels[i].localRotation = Quaternion.Euler(rot.x, rot.y, value);
        }
    }


}
