﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public interface DigitNumber {

    bool IsOn(DigitPart part);

}

public interface DigitPositions {
    Pourcent2D GetPositionFromBotLeftCorner(DigitPart part);
}
public enum DigitPart { Top, Center, Down, TopLeft, TopRight, DownLeft, DownRight }
public class Pourcent2D
{
    public float m_x;
    public float m_y;
}


public class UI_DigitPositions : MonoBehaviour, DigitPositions
{

    [Header("Parent boundary")]
    public Transform m_botLeftTransform;
    public Transform m_topRightTransform;
    [Header("Digit part Points")]
    public Transform m_top;
    public Transform m_center;
    public Transform m_down;
    public Transform m_topLeft;
    public Transform m_topRight;
    public Transform m_downLeft;
    public Transform m_downRight;
     public bool m_debugDraw;


    static DigitPart[] parts = new DigitPart[] { DigitPart.Top, DigitPart.TopRight, DigitPart.DownRight, DigitPart.Center, DigitPart.Down, DigitPart.DownLeft, DigitPart.TopLeft };

    public Transform GetTransform(DigitPart part) {
        switch (part)
        {
            case DigitPart.Top:return m_top;
            case DigitPart.Center: return m_center;
            case DigitPart.Down: return m_down;
            case DigitPart.TopLeft: return m_topLeft;
            case DigitPart.TopRight: return m_topRight;
            case DigitPart.DownLeft: return m_downLeft;
            case DigitPart.DownRight: return m_downRight;
        }
        return null;
    }

    

    public Pourcent2D GetPositionFromBotLeftCorner(DigitPart part) {

        Transform point = GetTransform(part);
        Transform botLeft = m_botLeftTransform;
        Vector3 position = botLeft.InverseTransformPoint(point.position);
        Pourcent2D pourcent = new Pourcent2D();
        pourcent.m_x = position.x / GetZoneWidth();
        pourcent.m_y = position.y / GetZoneHeight();

        return pourcent;
    }

    public void Update()
    {
        if (m_debugDraw) {

            Vector3 start = m_botLeftTransform.position;
            Debug.DrawLine(start, start + m_botLeftTransform.up * GetZoneHeight(), Color.green, Time.deltaTime);
            Debug.DrawLine(start, start + m_botLeftTransform.right * GetZoneWidth(), Color.green, Time.deltaTime);
            foreach (var item in parts)
            {
                Pourcent2D pct = GetPositionFromBotLeftCorner(item);
                Debug.DrawLine(start, start + (m_botLeftTransform.up * GetZoneHeight() * pct.m_y) + (m_botLeftTransform.right * GetZoneWidth() * pct.m_x), Color.red, Time.deltaTime);

            }
        }
    }

    public float GetZoneWidth()
    {
        Vector3 repositionTopRight = m_botLeftTransform.InverseTransformPoint(m_topRightTransform.position);
        return repositionTopRight.x;
    }
    public float GetZoneHeight()
    {

        Vector3 repositionTopRight = m_botLeftTransform.InverseTransformPoint(m_topRightTransform.position);
        return repositionTopRight.y;
    }

}
