﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AccessWebCamTexture : MonoBehaviour
    {

        public int m_webcamCount;
        public Texture2D m_image;
    public RawImage m_debug;
        public int m_width;
        public int m_height;
    public Vector2 m_autoFocus = new Vector2(0.5f, 0.5f);
    public float m_timebetweenRefresh=0.1f;

    public void SetAutoFocusX(float value)
    {
        m_autoFocus.x = value;


    }
    public void SetAutoFocusY(float value)
    {

        m_autoFocus.y = value;

    }
    public WebCamTexture webcamTexture;
    public Texture2D snap;
    IEnumerator Start()
        {

        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        WebCamDevice[] devices = WebCamTexture.devices;
         webcamTexture = new WebCamTexture();
        m_webcamCount = devices.Length;
        if (devices.Length > 0)
        {

            webcamTexture.deviceName = devices[0].name;
            webcamTexture.Play();
            m_debug.texture = webcamTexture;
            m_width = webcamTexture.width;
            m_height = webcamTexture.height;
            webcamTexture.autoFocusPoint = m_autoFocus;
            snap  = new Texture2D(webcamTexture.width, webcamTexture.height);
            yield return new WaitForSeconds(1);

            while (true)
            {
                yield return new WaitForEndOfFrame();
                yield return new WaitForSeconds( m_timebetweenRefresh);
                
                snap.SetPixels(webcamTexture.GetPixels());
                snap.Apply();
                m_image = snap;
                
            }
        }
            
    }
    public void Focus() {

        webcamTexture.autoFocusPoint = m_autoFocus;
    }

    }