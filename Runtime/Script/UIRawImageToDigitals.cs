﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRawImageToDigitals : MonoBehaviour
{

    public RawImage m_linkedImage;
    public UI_DigitPositions[] m_digitalsPosition;
    public DigitStateToString [] m_digitsToString;
    public float m_fullEmptyLimite = 0.5f;
    public bool m_blackIsEmpty=true;
    public Text m_displayResult;
    public float m_decimal = 1000000;
    [Header("Debug")]
    public Texture2D m_linkedTexture;
    public DigitGroup m_number;
    public string m_result;
    public float m_resultFloat;

    void Start()
    {
        m_number = new DigitGroup();
        m_number.m_digitsInfo = new DigitState[m_digitalsPosition.Length];
        for (int i = 0; i < m_number.m_digitsInfo.Length; i++)
        {
            m_number.m_digitsInfo[i] = new DigitState();

        }
    }

    public void SetBlackAsEmpty(bool value) {
        m_blackIsEmpty = value;
    }
    
    void Update()
    {

        m_linkedTexture = m_linkedImage.texture as Texture2D;
        if (m_linkedTexture == null)
            return;
        for (int i = 0; i < m_digitalsPosition.Length; i++)
        {
            for (int j = 0; j < parts.Length; j++)
            {
                Pourcent2D pct = m_digitalsPosition[i].GetPositionFromBotLeftCorner(parts[j]);
                Color c = m_linkedTexture.GetPixel((int)(m_linkedTexture.width * pct.m_x),(int)( m_linkedTexture.height * pct.m_y));
                bool isOn = c.r > m_fullEmptyLimite && c.g > m_fullEmptyLimite && c.b > m_fullEmptyLimite;

                m_number.m_digitsInfo[i].SetValue(m_blackIsEmpty?isOn:!isOn, parts[j]); 
            }

        }

        m_result = "";
        for (int i = 0; i < m_number.m_digitsInfo.Length; i++)
        {
            m_result += " " + GetCorrespondance(m_number.m_digitsInfo[i]);

        }
        m_result =m_result.Replace(" ", "");

        if(float.TryParse(m_result, out m_resultFloat))
        {
            m_resultFloat /= m_decimal;

        }

        if (m_displayResult) {
            m_displayResult.text =""+ m_resultFloat;
        }


    }

    public void SetPrecision(float value) {
        m_fullEmptyLimite = value;
    }
    private string GetCorrespondance(DigitState digitState)
    {
        for (int i = 0; i < m_digitsToString.Length; i++)
        {
            if (m_digitsToString[i].m_state.Equals( digitState)) {
                return m_digitsToString[i].m_linkedValue;
            }
        }
        return "0";
    }

    static DigitPart[] parts = new DigitPart[] { DigitPart.Top, DigitPart.TopRight, DigitPart.DownRight, DigitPart.Center, DigitPart.Down, DigitPart.DownLeft, DigitPart.TopLeft };
}
[System.Serializable]
public class DigitGroup
{
    public DigitState [] m_digitsInfo;
}
[System.Serializable]
public class DigitState
{
    public bool m_top       ;
    public bool m_center    ;
    public bool m_down      ;
    public bool m_topLeft   ;
    public bool m_topRight  ;
    public bool m_downLeft  ;
    public bool m_downRight ;

    public override bool Equals(object obj)
    {
        var state = obj as DigitState;
        return state != null &&
               m_top == state.m_top &&
               m_center == state.m_center &&
               m_down == state.m_down &&
               m_topLeft == state.m_topLeft &&
               m_topRight == state.m_topRight &&
               m_downLeft == state.m_downLeft &&
               m_downRight == state.m_downRight;
    }

    public override int GetHashCode()
    {
        var hashCode = -1633404427;
        hashCode = hashCode * -1521134295 + m_top.GetHashCode();
        hashCode = hashCode * -1521134295 + m_center.GetHashCode();
        hashCode = hashCode * -1521134295 + m_down.GetHashCode();
        hashCode = hashCode * -1521134295 + m_topLeft.GetHashCode();
        hashCode = hashCode * -1521134295 + m_topRight.GetHashCode();
        hashCode = hashCode * -1521134295 + m_downLeft.GetHashCode();
        hashCode = hashCode * -1521134295 + m_downRight.GetHashCode();
        return hashCode;
    }

    public override string ToString()
    {
        return ""
            + (m_top ? '1' : '0')
            + (m_center ? '1' : '0')
            + (m_down ? '1' : '0')
            + (m_topLeft ? '1' : '0')
            + (m_topRight ? '1' : '0')
            + (m_downLeft ? '1' : '0')
            + (m_downRight ? '1' : '0');
    }

    internal void SetValue(bool value, DigitPart digitPart)
    {
        switch (digitPart)
        {
            case DigitPart.Top:         m_top = value; break;      
            case DigitPart.Center: m_center = value; break;
            case DigitPart.Down: m_down = value; break;
            case DigitPart.TopLeft: m_topLeft = value; break;
            case DigitPart.TopRight: m_topRight = value; break;
            case DigitPart.DownLeft: m_downLeft = value; break;
            case DigitPart.DownRight: m_downRight = value; break;
        }
    }
}
[System.Serializable]
public class DigitStateToString {
    public string m_linkedValue;
    public DigitState m_state;
}