﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebcamToMQTT_DIRTY : MonoBehaviour
{
    public AccessWebCamTexture m_texture;
    public RawImage m_debugRawImage;
    public AspectRatioFitter m_ratioFitter;
    
    void Update()
    {
        if (m_texture != null && m_texture.m_image!=null) {
            m_ratioFitter.aspectRatio = (float) m_texture.m_width / (float) m_texture.m_height;
            m_debugRawImage.texture = m_texture.m_image;
        }
    }
}
