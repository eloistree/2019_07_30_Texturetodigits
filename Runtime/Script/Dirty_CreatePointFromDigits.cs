﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dirty_CreatePointFromDigits : MonoBehaviour
{
    public GameObject m_prefab;
    public Transform m_laserOrigine;
    public UIRawImageToDigitals m_distance;
    public Transform m_parent;
    public float m_lastDistance;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_lastDistance =m_distance.m_resultFloat;
        if (Input.GetMouseButtonDown(0)) {
            GameObject obj = Instantiate(m_prefab, m_parent);
            obj.transform.position = m_laserOrigine.position;
            obj.transform.forward = m_laserOrigine.forward;
            obj.transform.Translate(Vector3.forward * m_lastDistance, Space.Self);
        }
    }
}
