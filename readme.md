# How to use: 2019_07_30_Texturetodigits   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.texturetodigits":"",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.texturetodigits",                              
  "displayName": "Texture to digits",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Recover digitals information from a texture or a webcam.",                         
  "keywords": ["Script","Tool","Digits","Webcam","Reader","Texture"],                       
  "category": "Script",                   
  "dependencies":{}     
  }                                                                                
```    